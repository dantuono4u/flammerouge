package jeu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * Classe Piste
 */
public class Piste{
	//Attribut donnant la taille de la piste en tuile
	public static int tailleTab;
	//Attribut correspondant � la Piste en tableau de Tuiles
	private Tuile[] tuiles;
	// Attribut permetant de connaitre le dernier Coureur de la course
	private Coureur dernier=null;
	
	/**
	 * constructeur de la Piste
	 * @param fichier chemin de la piste a charger
	 */
	public Piste(String fichier) {
		try {
			this.fichierPiste(fichier);
		} catch (IOException r) {
			this.chemin="../Piste/";
			try {
				this.fichierPiste(fichier);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("vous devez vous placer dans le fichier bin");
			}
		}
		
	}
	
	/**M�thode permettant de lire et de construire une Piste � 
	 * partir d'un fichier texte
	 * @param String emplacement du fichier
	 */
	public void fichierPiste(String fichier)throws IOException{ 
		//initialisation de la lecture
		BufferedReader reader = new BufferedReader(new FileReader("Piste/"+fichier));
		String ligne;
		int i = 0;
		tailleTab = Integer.valueOf(reader.readLine());
		this.tuiles = new Tuile[tailleTab];
		//lecture et insertion des �l�ments dans la Piste 
		while((ligne = reader.readLine()) != null && i < tailleTab){
			switch(ligne){
				//ajout d'une ligne droite
				case "LD":
					this.tuiles[i] = new LigneDroite();
					break;
				//ajout d'un d�part
				case "DE":
					this.tuiles[i] = new Depart();
					break;
				//ajout d'une ligne d'arriv�e
				case "AR":
					this.tuiles[i] = new Arrivee();
					break;
				//ajout d'un virage l�g�
				case "VL":
					this.tuiles[i] = new VirageLeger();
					break;
				//ajout d'un virage serr�
				case "VS":
					this.tuiles[i] = new VirageSerre();
					break;
				//ajout d'une mont�e
				case "MV":
					this.tuiles[i] = new Montagnes(true);
					break;
				//ajout d'une descente
				case "MF":
					this.tuiles[i] = new Montagnes(false);
			}
			
			i++;
		}
		//fermeture du flux de lecture
		reader.close();
	}
	/**
	 * Getter de la Tuile D�part
	 * @return Depart retourne la tuile de d�part de la piste
	 */
	public Depart getDepart(){
		return (Depart) this.tuiles[0];
	}
	
	/**
	 * Getter de la Piste
	 * @return Tuile[] retourne le tableau de Tuiles repr�sentant la Piste
	 */
	public Tuile[] getTabTuiles(){
		return this.tuiles;
	}
	
	/**
	 * Getter d'une Tuile
	 * @return Tuile retourne une tuile de la Piste
	 * @param Int prend en paramatre l'indice de la Tuile voulu
	 */
	public Tuile getTuile(int i) {
		if(i >= this.tailleTab || i < 0) {
			return null;
		}else {
			return this.tuiles[i];
		}
	}
	
	/**
	 * Getter d'une Tuile
	 * @return Tuile retourne une tuile de la Piste y compris une Tuile du D�part
	 * @param Int prend en paramatre l'indice de la Tuile voulu y compris une Tuile du D�part
	 */
	public Tuile getTuileAvecDep(int i) {
		if(i >= this.tailleTab || i < -3) {
			return null;	
		}else if(i <= 0 && i >= -3){
			return ((Depart) (this.tuiles[0])).getTuileDepart(i);
		}else {
			return this.tuiles[i];
		}
	}
	
/**
 * methode permettant de v�rifi� si une Tuile correspond au D�part
 * @return boolean vrai si la Tuile est de type Depart
 */
	public boolean estDepart(int i) {
		if(i <= 0) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Getter de la taille de la Piste
	 * @return Int taille du tableau
	 */
	public int getTaille() {
		return this.tailleTab;
	}
	
/**
 * M�thode permattant de r�cup�rer l'indice de la Piste o� se trouve un Coureur
 * @param Coureur Coureur recherch�
 * @return Int Indice du Coureur
 */
	public int getIndex(Coureur c) {
		int pos = 0;
		//parcours du tableau
		for (int i = 0; i < tuiles.length; i++) {
			if(this.getTuile(i).getVoieGauche() == c|| this.getTuile(i).getVoieDroite() == c) {
				pos = i;
				break;
			}
		}
		
		return pos;	
	}
	
}
