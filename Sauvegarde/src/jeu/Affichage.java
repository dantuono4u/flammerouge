
package src.jeu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Affichage extends JPanel {
	private Jeu j;
	private int hauteur;
	private int largeur;
	public  Affichage(Jeu j,int h, int l){
		this.j=j;
		setPreferredSize(new Dimension (h ,l));
		hauteur=h;
		largeur=l;
		
	}
	public void repainte() {
		this.repaint();
	}
	public  void  paintComponent(Graphics g) {
		super.paintComponent(g);
		hauteur=getHeight();
		largeur=getWidth();
		g.setFont(new Font("Arial", Font.PLAIN, 40));		
		Tuile[] p = j.getPiste().getTabTuiles();
		Tuile[] dep = j.getPiste().getDepart().getDepart();
		int x=100, y=50, sens=1;

		for(int i=0; i<dep.length;i++) {
			g.setColor(Color.magenta);
			g.fillRect(x, y, 40, 80);
			if(dep[i].getVoieDroite()!=null) {
				Coureur c=dep[i].getVoieDroite();
				g.setColor(Couleur.color[c.getCouleur()]);
				g.drawString(c.getType(), x+5, y+75);
				g.drawRect(x, y+40, 40, 40);
			}
			if(dep[i].getVoieGauche()!=null) {
				Coureur c=dep[i].getVoieGauche();
				g.setColor(Couleur.color[c.getCouleur()]);
				g.drawString(c.getType(), x+5, y+35);
				g.drawRect(x, y, 40, 40);
				
			}
			x+=41*sens;
		}
		for(int i=1; i<p.length;i++) {
			g.setColor(new Color(206,206,206));
			if(x>largeur-100 || x<100) {
				sens*=-1;
				y+=100;
			}
			g.fillRect(x, y, 40, 80);
			if(p[i] instanceof  Montagnes) {
				if(((Montagnes) p[i]).getMontee())
					g.setColor(Color.RED);
				else
					g.setColor(Color.BLUE);
				if(p[i].getVoieDroite()==null)
					g.drawRect(x, y, 40, 80);
			}
			if(p[i] instanceof Arrivee) {
					g.setColor(Color.YELLOW);
					g.fillRect(x, y, 40, 80);
			}
			if(p[i].getVoieGauche()!=null) {
				Coureur c=p[i].getVoieGauche();
				g.setColor(Couleur.color[c.getCouleur()]);
				g.drawString(c.getType(), x+5, y+35);
				g.drawRect(x, y, 40, 40);
			}
			if(p[i].getVoieDroite()!=null) {
				Coureur c=p[i].getVoieDroite();
				g.setColor(Couleur.color[c.getCouleur()]);
				g.drawString(c.getType(), x+5, y+75);
				g.drawRect(x, y+40, 40, 40);
			}
			x+=41*sens;
			
		}
	}
}
