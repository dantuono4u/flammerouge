package src.jeu;

import java.util.ArrayList; 
import java.util.Collections;

/**
 * Classe CarteRouleur
 *
 */
public class CarteRouleur extends CarteCycliste{
	
	
	public CarteRouleur(){
		super();
		pioche=new ArrayList<Integer>();
		defausse = new  ArrayList<Integer>();
		//Initialisation de la pioche du Rouleur
		for(int i=3; i<8; i++){
			for(int j=0; j<nbcartes; j++)
				pioche.add(i);
		}
		//M�lange de la pioche
		Collections.shuffle(pioche);
	}
	
	public int somme(int[] tab){
		int s=0;
		for(int i=0; i<tab.length; i++){
			s+=tab[i];
		}
		return s;
	}
}
