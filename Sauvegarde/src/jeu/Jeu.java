package jeu;


import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.Position;
/**
 * Classe Jeu
 * D�roulement du Jeu
 **/
public class Jeu {
	//Attributs priv�s de Jeu
	private int nbJoueurs;
	private String[] nomJoueurs;
	private Coureur[] sprinteurs;
	private Coureur[] rouleurs;
	private Piste piste;
	private Classement classement;
	private boolean continu;

	/**
	 * Constructeur
	 **/
	public Jeu() {
		//Initialisation du Jeu
		Scanner sc = new Scanner(System.in);
		continu=true;
		
		boolean erreur1 = true;
		while(erreur1) {
			System.out.println("Entrez le nombre de joueurs : (2-4)");
			try {
				this.nbJoueurs = sc.nextInt();
				if(this.nbJoueurs >= 2 && this.nbJoueurs <= 4) {
					erreur1 = false;
				}else {
					System.out.println("Erreur : nombre de joueur incorrect");
				}
			} catch (Exception e) {
				System.out.println("Erreur : vous devez entrer en entier");
			}
			sc.nextLine();
		}
		
		this.nomJoueurs = new String[nbJoueurs];
		this.sprinteurs = new Coureur[nbJoueurs];
		this.rouleurs = new Coureur[nbJoueurs];
		//Choix de la Piste
		System.out.println("Piste1, Piste2, Piste3 ou Montana?");
		String choixPiste = sc.nextLine();
		while(!choixPiste.equals("Piste1")&&!choixPiste.equals("Piste2")&&!choixPiste.equals("Piste3")&&!choixPiste.equals("Montana")) {
			System.out.println("Tapez 'Piste1', 'Piste2', 'Piste3' ou 'Montana");
			choixPiste=sc.nextLine();
		}
		this.piste = new Piste(choixPiste);
		//Constructions des Joueurs et de leurs Coureurs
		for(int i = 0; i < nbJoueurs; i++){
			System.out.println("Entrez le nom du joueur " + Couleur.tabCouleur[i] + " : " );
			String nom = sc.nextLine();
			this.nomJoueurs[i] = nom;
			this.sprinteurs[i] = new Coureur("S", i);
			boolean placerS = false, placerR = false;
			//placement du Sprinteur
			while(!placerS) {
				int place = 0;
				while(place < 1 || place > 8) {
					System.out.println("Placer votre sprinter : (1-8)");
					try {
						place = sc.nextInt();
						if(place < 1 || place > 8) System.out.println("Erreur : entrez un entier entre 1 et 8");
					} catch (Exception e) {
						System.out.println("Erreur : entrez un entier ");
					}
					
					sc.nextLine();
				}
				
				placerS = this.piste.getDepart().placer(this.sprinteurs[i], place);
				if(!placerS) {
					System.out.println("La case est deja occupee !");
				}
			}
			this.rouleurs[i] = new Coureur("R", i);
			//placement du Rouleur
			while(!placerR) {
				int place = 0;
				while(place < 1 || place > 8) {
					System.out.println("Placer votre rouleur : (1-8)");
					try {
						place = sc.nextInt();
						if(place < 1 || place > 8) System.out.println("Erreur : entrez un entier entre 1 et 8");
					} catch (Exception e) {
						System.out.println("Erreur : entrez un entier");
					}	
				
					sc.nextLine();
				}
				placerR = this.piste.getDepart().placer(this.rouleurs[i], place);
				if(!placerR) {
					System.out.println("La case est deja occupee !");
				}
			}
		}
		System.out.println("Preparation du jeu terminee");
		//Initialistaion du Classement
		this.classement = new Classement(this);	
	}
	public boolean isContinu() {
		return continu;
	}
	public void setContinu(boolean continu) {
		this.continu = continu;
	}
	public Classement getClassement() {
		return classement;
	}
	public void setClassement(Classement classement) {
		this.classement = classement;
	}
	/**
	 * M�thode pour la Phase d'�nergie
	 **/
	public void phaseE(){
		Scanner sc = new Scanner(System.in);
		CarteCycliste carte;
		int[] tabPioche=new int[4];
		int choix;
		for(int i=0; i<nbJoueurs; i++){
			carte = this.sprinteurs[i].getListeCartes();
			System.out.println("Pioche joueur " + nomJoueurs[i]  + "(" + Couleur.tabCouleur[this.sprinteurs[i].getCouleur()] + ") sprinteur :");
			//pioche des 4 Cartes �nergies du Sprinteurs
			for(int j=0; j<4; j++)
				tabPioche[j]= carte.piocher();
			//choix de la carte �nergie du Sprinteur
			choix = 0;
			do{
				System.out.println("choisir sa carte :" + "\ncarte 1: " + tabPioche[0]+"\ncarte 2: "+tabPioche[1]+"\ncarte 3: "+tabPioche[2]+"\ncarte 4: "+tabPioche[3]+"\n");
				try {
					choix = sc.nextInt();
					
				} catch (Exception e) {
					System.out.println("vous devez entrer le numero de la carte");
				}
				sc.nextLine();
				
				if(choix>4 || choix<1)
					System.out.println(choix + " ne correspond pas � une carte !");
			}while(choix>4 || choix<1);
			//si sprinteur sur une montagne, carte �nergie < 5, c'est une descente alors changement de carte en une carte 5
			if(this.sprinteurs[i].getPosition()>0 && piste.getTuile(this.sprinteurs[i].getPosition()) instanceof Montagnes && tabPioche[choix-1]<5 && !((Montagnes) piste.getTuile(this.sprinteurs[i].getPosition())).getMontee()){
				tabPioche[choix-1]=5;
		}
			//Stockage de la Carte choisie
			carte.setCarteE(tabPioche[choix-1]);
			System.out.println(carte.getCarteE());
			//d�fausse des cartes pioch�es
			for(int h=0; h<4; h++){
				carte.defausser(tabPioche[h]);
			}
			carte = this.rouleurs[i].getListeCartes();
			System.out.println("Pioche joueur " + nomJoueurs[i] + "(" + Couleur.tabCouleur[this.rouleurs[i].getCouleur()] + ") rouleur :");
			//pioche des 4 Cartes �nergies du Rouleur
			for(int j=0; j<4; j++)
				tabPioche[j]= carte.piocher();
			//choix de la carte �nergie du Rouleur
			do{
				System.out.println("choisir sa carte :" + "\ncarte 1: " + tabPioche[0]+"\ncarte 2: "+tabPioche[1]+"\ncarte 3: "+tabPioche[2]+"\ncarte 4: "+tabPioche[3]+"\n");
				choix = sc.nextInt();
				if(choix>4 || choix<1)
					System.out.println(choix + " ne correspond pas � une carte !");
			}while(choix>4 || choix<1);
			//si rouleur sur une Montagne, carte �nergie < 5, c'est une descente alors changement de carte en une carte 5
			if(this.rouleurs[i].getPosition()>0 && piste.getTuile(this.rouleurs[i].getPosition()) instanceof Montagnes && tabPioche[choix-1]<5 && !((Montagnes) piste.getTuile(this.rouleurs[i].getPosition())).getMontee())
					tabPioche[choix-1]=5;
			//Stockage de la Carte choisie
			carte.setCarteE(tabPioche[choix-1]);
			System.out.println(carte.getCarteE());
			//d�fausse des cartes pioch�es
			for(int j=0; j<4; j++){
				if(j!=(choix-1))
					carte.defausser(tabPioche[j]);
			}
		}
	}
	
	/**
	 * M�thode pour l'ajout de la carte fatigue
	 **/
	public void ajoutCarteFatigue() {
		//mise � jour du classement pour l'utilisation apr�s
		classement.miseAjour();
		//parcours du classement
		for (int i = 0; i < classement.getClassement().length; i++) {
			int pos = this.classement.getClassement()[i].getPosition();
			//condition de l'ajout de la carte fatigue
			if(this.piste.getTuileAvecDep(pos+1).getVoieDroite() == null && this.piste.getTuileAvecDep(pos+1).getVoieGauche() == null) {
				//ajout de la carte fatigue
				this.classement.getClassement()[i].getListeCartes().ajoutFatigue();
				System.out.println(classement.getClassement()[i] + "a pris une carte fatigue");
			}
		}
	}

	/**
	 * Getter nombre de Joueurs
	 **/
	public int getNbJoueurs() {
		return nbJoueurs;
	}

	/**
	 * Setter nombre de Joueurs
	 **/
	public void setNbJoueurs(int nbJoueurs) {
		this.nbJoueurs = nbJoueurs;
	}

	/**
	 * M�thode de la phase de mouvement
	 **/
	public void phaseM() {
		//Mise � jour du classement pour l'utilisation
		classement.miseAjour();
		//parcours des Coureurs
		for(int i = 0; i < nbJoueurs*2; i++) {
			Coureur coureur = classement.getClassement()[i];
			int nbavance = 0;
			boolean dejaMontee=false;
			while(coureur.getListeCartes().getCarteE() > 0 && coureur.getPosition()<piste.getTaille()) {
				nbavance++;
				if(coureur.getPosition() +1 >= piste.getTaille())
					break;
				
				if(piste.getTuile(coureur.getPosition()) instanceof Montagnes)
					dejaMontee=true;
				if(piste.getTuile(coureur.getPosition()+1) instanceof Montagnes && nbavance>4 && !dejaMontee) {
					coureur.getListeCartes().setCarteE(0);
					break;
				}
				if(piste.getTuile(coureur.getPosition()) instanceof Montagnes && ((Montagnes) piste.getTuile(coureur.getPosition())).getMontee() && coureur.getListeCartes().getCarteE() > 5)
					coureur.getListeCartes().setCarteE(5);
				if(coureur.getPosition() < 0) {
					//si on est au depart
					if(coureur.getVoie() == 'd') {
						//si on est a droite
						if(this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).getVoieDroite() == null) {
							//si on peut aller a droite
							this.piste.getDepart().getTuileDepart(coureur.getPosition()).setVoieDroite(null);
							this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).setVoieDroite(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('d');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else if(this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).getVoieGauche() == null) {
							//si on peut aller a gauche
							this.piste.getDepart().getTuileDepart(coureur.getPosition()).setVoieDroite(null);
							this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).setVoieGauche(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					}else if(coureur.getVoie() == 'g') {
						//si on est a gauche
						if(this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).getVoieDroite() == null) {
							//si on peut aller a droite
							this.piste.getDepart().getTuileDepart(coureur.getPosition()).setVoieGauche(null);
							this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).setVoieDroite(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('d');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else if(this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).getVoieGauche() == null) {
							//si on peut aller a gauche
							this.piste.getDepart().getTuileDepart(coureur.getPosition()).setVoieGauche(null);
							this.piste.getDepart().getTuileDepart(coureur.getPosition()+1).setVoieGauche(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					}
				}else if(coureur.getPosition() == 0) {	
					//si on est a la derniere case depart
					if(coureur.getVoie() == 'd') {
						//si on est a droite
						if(this.piste.getTuile(1).getVoieDroite() == null) {
							//si on peut aller a droite
							this.piste.getDepart().getTuileDepart(0).setVoieDroite(null);
							this.piste.getTuile(1).setVoieDroite(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('d');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else if(this.piste.getTuile(1).getVoieGauche() == null) {
							//si on peut aller a gauche
							this.piste.getDepart().getTuileDepart(0).setVoieDroite(null);
							this.piste.getTuile(1).setVoieGauche(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					}else if(coureur.getVoie() == 'g'){
						//si on est a gauche
						if(this.piste.getTuile(1).getVoieDroite() == null) {
							//si on peut aller a droite
							this.piste.getDepart().getTuileDepart(0).setVoieGauche(null);
							this.piste.getTuile(1).setVoieDroite(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('d');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else if(this.piste.getTuile(1).getVoieGauche() == null) {
							//si on peut aller a gauche
							this.piste.getDepart().getTuileDepart(0).setVoieGauche(null);
							this.piste.getTuile(1).setVoieGauche(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					}
					//si on est pas au depart
				}else {
					//si on est a droite
					if(coureur.getVoie() == 'd') {
						//si on peut avancer
						if(this.piste.getTuile(coureur.getPosition()+1).getVoieDroite() == null) {
							//tuile actuelle devient vide
							this.piste.getTuile(coureur.getPosition()).setVoieDroite(null);
							//tuile suivante prend le coureur
							this.piste.getTuile(coureur.getPosition()+1).setVoieDroite(coureur);
							//changement position coureur
							coureur.setPosition(coureur.getPosition()+1);
							//diminue la carte �nergie d'un
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						//si on peut aller a gauche
						}else if(this.piste.getTuile(coureur.getPosition()+1).getVoieGauche() == null) {
							//tuile actuelle devient vide
							this.piste.getTuile(coureur.getPosition()).setVoieDroite(null);
							//tuile de gauche r�cup�re le coureur
							this.piste.getTuile(coureur.getPosition()+1).setVoieGauche(coureur);
							//changement position coureur
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							//diminue la carte �nergie d'un
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					//si on est a gauche
					}else if(coureur.getVoie() == 'g'){
						//si on peut aller a droite
						if(this.piste.getTuile(coureur.getPosition()+1).getVoieDroite() == null) {
							//tuile actuelle devient vide
							this.piste.getTuile(coureur.getPosition()).setVoieGauche(null);
							//tuile de droite prend le coureur
							this.piste.getTuile(coureur.getPosition()+1).setVoieDroite(coureur);
							//changement de position du coureur
							coureur.setPosition(coureur.getPosition()+1);
							//changement voie coureur
							coureur.setVoie('d');
							//diminue la carte �nergie d'un
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);
						}else if(this.piste.getTuile(coureur.getPosition()+1).getVoieGauche() == null) {
							//si on peut aller a gauche
							this.piste.getTuile(coureur.getPosition()).setVoieGauche(null);
							this.piste.getTuile(coureur.getPosition()+1).setVoieGauche(coureur);
							coureur.setPosition(coureur.getPosition()+1);
							coureur.setVoie('g');
							coureur.getListeCartes().setCarteE(coureur.getListeCartes().getCarteE()-1);		
						}else {
							coureur.getListeCartes().setCarteE(0);
						}
					}else {
						System.err.println("Erreur deplacement joueur");
					}
				}
				
			}
		}
	}

	/**
	 * M�thode Aspiration
	 **/
	public void Aspiration(){
		//Mise � jour du classement pour l'utilisation
		this.classement.miseAjour();
		ArrayList<Integer> groupes = new ArrayList<Integer>();
		//r�cup�ration du classement
		Coureur[] cla = classement.getClassement();
		//cr�ation des groupes
		int debutGr = cla[cla.length-1].getPosition();
		for (int i = cla.length-1; i >= 0; i--) {
			if(cla[i].getPosition() == debutGr || cla[i].getPosition() == debutGr + 1) {
				debutGr = cla[i].getPosition();
			}else {
				groupes.add(debutGr);
				debutGr = cla[i].getPosition();
			}
		}
		groupes.add(debutGr);
		//Aspiration des groupes
		while(groupes.size() > 0) {
			int deb = groupes.get(0);
			if(deb +2 < this.piste.getTaille()) {
				if(this.piste.getTuileAvecDep(deb+1).getVoieDroite() == null && this.piste.getTuileAvecDep(deb+1).getVoieGauche() == null && (this.piste.getTuileAvecDep(deb+2).getVoieGauche() != null || this.piste.getTuileAvecDep(deb+2).getVoieDroite()!= null)) {
					while(this.piste.getTuileAvecDep(deb).getVoieDroite() != null || this.piste.getTuileAvecDep(deb).getVoieGauche() != null) {
						if(this.piste.getTuileAvecDep(deb).getVoieDroite() != null && (!(this.piste.getTuile(deb) instanceof Montagnes) || ((Montagnes) this.piste.getTuile(deb)).getMontee() == false)) {
							if(!(this.piste.getTuile(deb+1) instanceof Montagnes) || (this.piste.getTuile(deb+1) instanceof Montagnes && ((Montagnes) this.piste.getTuile(deb+1)).getMontee() == false)) {
								if(this.piste.getTuileAvecDep(deb+1).getVoieDroite() == null) {
									this.piste.getTuileAvecDep(deb+1).setVoieDroite(this.piste.getTuileAvecDep(deb).getVoieDroite());
									this.piste.getTuileAvecDep(deb).getVoieDroite().setPosition(this.piste.getTuileAvecDep(deb).getVoieDroite().getPosition()+1);
									this.piste.getTuileAvecDep(deb).setVoieDroite(null);
								}
							}
						}
						
						if(this.piste.getTuileAvecDep(deb).getVoieGauche() != null) {
							if(!(this.piste.getTuile(deb+1) instanceof Montagnes) || (this.piste.getTuile(deb+1) instanceof Montagnes && ((Montagnes) this.piste.getTuile(deb+1)).getMontee() == false)) {
								if(this.piste.getTuileAvecDep(deb+1).getVoieGauche() == null) {
									this.piste.getTuileAvecDep(deb+1).setVoieGauche(this.piste.getTuileAvecDep(deb).getVoieGauche());
									this.piste.getTuileAvecDep(deb).getVoieGauche().setPosition(this.piste.getTuileAvecDep(deb).getVoieGauche().getPosition()+1);
									this.piste.getTuileAvecDep(deb).setVoieGauche(null);
								}
							}
						}
						deb--;
						this.classement.miseAjour();
					}				
				}	
				groupes.remove(0);
			}
		}
	}
	
	/**
	 * M�thode pour l'affichage de la piste
	 **/
	public void AffichagePiste() {
		String voieGauche = "|";
		String voieDroite = "|";

		for (int i = 0; i < this.piste.getTaille(); i++) {
			if(this.piste.getTuile(i) instanceof Arrivee) {
				voieGauche += "@|";
				voieDroite += "@|";
			}else if(this.piste.getTuile(i) instanceof Depart) {
				Tuile[] dep = ((Depart) this.piste.getTuile(i)).getDepart();
				for (int j = 0; j < 4; j++) {
					Coureur gauche = dep[j].getVoieGauche();
					if(gauche == null) {
						voieGauche += "         |";
					}else {
						if(gauche.getType().equals("S")){
							voieGauche += Couleur.CouleurS[gauche.getCouleur()];
							voieGauche += " |";
						}
						else{
							voieGauche += Couleur.CouleurR[gauche.getCouleur()];
							voieGauche += " |";
						}
					}

					Coureur droite = dep[j].getVoieDroite();
					if(droite == null) {
						voieDroite += "         |";
					}else {
						if(droite.getType().equals("S")){
							voieDroite += Couleur.CouleurS[droite.getCouleur()];
							voieDroite += " |";
						}
						else{
							voieDroite += Couleur.CouleurR[droite.getCouleur()];
							voieDroite += " |";
						}
					}
				}
				voieGauche+=("\\");
				voieDroite+=("/");

			}else {
				Coureur gauche = this.piste.getTuile(i).getVoieGauche();
				if(gauche == null) {
					voieGauche += "         |";
				}else {
					if(gauche.getType().equals("S")){
						voieGauche += Couleur.CouleurS[gauche.getCouleur()];
						voieGauche += " |";
					}
					else{
						voieGauche += Couleur.CouleurR[gauche.getCouleur()];
						voieGauche += " |";
					}
				}
				Coureur droite = this.piste.getTuile(i).getVoieDroite();
				if(droite == null) {
					voieDroite += "         |";
				}else {
					if(droite.getType().equals("S")){
						voieDroite += Couleur.CouleurS[droite.getCouleur()];
						voieDroite += " |";
					}
					else{
						voieDroite += Couleur.CouleurR[droite.getCouleur()];
						voieDroite += " |";
					}
				}	

			}
		}
		String voie="";
		for(int j=0; j<voieGauche.length();j++)
			voie+="=";
		System.out.println(voie);
		System.out.println(voieGauche);
		System.out.println(voieDroite);
		System.out.println(voie);

	}
	/**
	 * Getter de la Piste
	 * @return Piste Piste du jeu
	 */
	public Piste getPiste() {
		return this.piste;
	}

	/**
	 * Getter des Sprinteurs
	 * @return Coureur[] retourne tous les Sprinteurs
	 */
	public Coureur[] getSprinteurs() {
		return this.sprinteurs;
	}

	/**
	 * Getter des Rouleurs
	 * @return Coureur[] retourne tous les Rouleurs
	 */
	public Coureur[] getRouleurs() {
		return this.rouleurs;
	}
	
	/**
	 * M�hode mettant fin � la partie
	 * @param gagnant
	 */
	public void finPartie(Coureur gagnant) {
		this.continu = false;
		AffichagePiste();
		System.out.println(this.nomJoueurs[gagnant.getCouleur()] + " a gagne avec le pion"+ gagnant + "! \n");
	}
	
	public void clearConsole() throws IOException {
		Runtime.getRuntime().exec("cls");
	}

}