package src.jeu;

import java.util.ArrayList;
import java.util.Arrays;
/**
 * Classe Classement
 * Classe les Coureurs dans un tableau du premier au deernier
 */
public class Classement {
	//Attrubuts de la classe
	private Coureur[] classement;
	private Piste p;
	private Jeu j;
	
	/**
	 * Constructeur
	 */
	public Classement(Jeu jeu){
		this.j = jeu;
		this.p = this.j.getPiste();
		int nbC = j.getNbJoueurs()*2;
		classement = new Coureur[nbC];
		//mise � jour du Classement 
		miseAjour();
	}
	
	/**
	 * M�thode de mise � jour du classement
	 */
	public void miseAjour(){
		int pos = this.j.getNbJoueurs()*2-1;
		//Parcours de la Piste du jeu
		for (int i = 0; i < this.j.getPiste().getTaille(); i++) {
			//si c'est le Depart
			if(i == 0) {
				//parcours du Depart et ajout dans le classement
				Tuile[] tab = this.j.getPiste().getDepart().getDepart();
				for (int j = 0; j < tab.length; j++) {
					if(tab[j].getVoieGauche() != null && pos >= 0) {
						this.classement[pos] = tab[j].getVoieGauche();
						pos--;
					}
					if(tab[j].getVoieDroite() != null && pos >= 0) {
						this.classement[pos] = tab[j].getVoieDroite();
						pos--;
					}
				}
			}else {
				//Parcours de la Tuile et ajout pour la voie gauche
				if(this.j.getPiste().getTuile(i).getVoieGauche() != null && pos >= 0) {
					this.classement[pos] = this.j.getPiste().getTuile(i).getVoieGauche();
					pos--;
				}
				//Parcours de la Tuile et ajout pour la voie droite
				if(this.j.getPiste().getTuile(i).getVoieDroite() != null && pos >= 0) {
					this.classement[pos] = this.j.getPiste().getTuile(i).getVoieDroite();
					pos--;
				}
			}
		}
	}
	/**
	 * Getter du tableau du classement
	 * @return
	 */
	public Coureur[] getClassement(){
		return classement;
	}
	
	/**
	 *Getter du premier Coureur 
	 */
	public Coureur getPremier(){
		return classement[0];
	}
	
	public String toString() {
		return Arrays.toString(classement);
	}
	
}
