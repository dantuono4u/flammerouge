package jeu;

import  javax.swing .*;
import  java.awt .*;
import  java.util .*;

public  class  Principale {
	public  static  void  main(String [] args) {
		Jeu jeu = new Jeu();
		JFrame  fenetre=new  JFrame("Affichage Piste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
		Affichage  dessin=new  Affichage(jeu, 800,400);
		dessin.setBackground(new Color(167, 103, 38));
		fenetre.setContentPane(dessin);
		fenetre.pack ();
		fenetre.setVisible(true);
		while(jeu.isContinu()){
			System.out.println("\n->Tirage des cartes : ");
			jeu.AffichagePiste();
			//Phase de choix de la carte d'�nergie
			jeu.phaseE();
			System.out.println("\n->Mouvement");
			jeu.AffichagePiste();
			//Phase de mouvement
			jeu.phaseM();
			jeu.AffichagePiste();
			System.out.println("\n->Aspiration");
			//Phase d'Aspiration
			jeu.Aspiration();
			jeu.AffichagePiste();
			System.out.println("\n->Ajout des cartes fatigues : ");
			//Phase Fatigue
			jeu.ajoutCarteFatigue();
			jeu.getClassement().miseAjour();
			int posPremier = jeu.getClassement().getPremier().getPosition();
			dessin.repainte();
			//si un joueur � fini la course, fin de la partie
			if(jeu.getPiste().getTuile(posPremier) instanceof Arrivee) {
				jeu.finPartie(jeu.getClassement().getPremier());
			}}
		dessin.repainte();
	}
}