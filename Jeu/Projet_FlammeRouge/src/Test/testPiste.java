package Test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import org.junit.Test;

import Class.*;

public class testPiste {

	/**
	 * Test du Constructeur
	 */
	@Test
	public void testConstructeur(){
		//preparation des donn�es
		Piste p1 = new Piste("PisteTest");
		String[] tuiles = {"LD","VS","VL","MV","MF"};
		Piste p2 = new Piste(tuiles);
		//validation du r�sultat
		assertEquals("Les tailles devrait �tre les memes", p1.getTaille(), p2.getTaille());
		//preparation des donn�es
		boolean egaux = true;
		for(int i=0; i<p1.getTaille();i++) {
			if(! p1.getTuile(i).getClass().getName().equals(p2.getTuile(i).getClass().getName()) )
				egaux=false;
		}
		//validation du r�sultat
		assertTrue("Les pistes devrait �tre les memes", egaux);
	}

	/**
	 * Test du Getter d'une Tuile
	 */
	@Test
	public void testGetTuile() {
		//preparation des donn�es
		String[] tuiles = {"LD","VS","VL","MV","MF"};
		Piste p2 = new Piste(tuiles);
		//validation du r�sultat
		assertTrue("La Tuile devrait �tre un virage l�g�", p2.getTuile(3) instanceof VirageLeger);
	}

	/**
	 * Test du Getter d'une Tuile avec le d�part
	 */
	@Test
	public void testGetTuileAvecDep() {
		//preparation des donn�es
		String[] tuiles = {"LD","VS","VL","MV","MF"};
		Piste p2 = new Piste(tuiles);
		Coureur c1 = new Coureur("Jean", 3);
		Coureur c2 = new Coureur("Pierre", 3);
		Coureur c3 = new Coureur("Paul", 3);
		p2.getDepart().placer(c1, 8);
		p2.getDepart().placer(c2, 1);
		p2.getTuile(5).setVoieGauche(c3);
		//validation du r�sultat
		assertEquals("La Tuile devrait contenir le coureur Jean", p2.getTuileAvecDep(0).getVoieDroite(), c1);
		assertEquals("La Tuile devrait contenir le coureur Pierre", p2.getTuileAvecDep(-3).getVoieGauche(), c2);
		assertEquals("La Tuile devrait contenir le coureur Paul", p2.getTuileAvecDep(5).getVoieGauche(), c3);
		assertEquals("La methode ne devrait pas retourner de Tuile", p2.getTuileAvecDep(-4), null);
		assertEquals("La methode ne devrait pas retourner de Tuile", p2.getTuileAvecDep(20), null);
	}

	/**
	 * Test de la v�rification du d�part
	 */
	@Test
	public void testEstDepart() {
		//preparation des donn�es
		Piste p = new Piste("Piste1");
		//validation du r�sultat
		assertTrue("La tuile devrait �tre un d�part", p.estDepart(0));
		assertFalse("La tuile ne devrait pas �tre un d�part", p.estDepart(3));
		assertFalse("La tuile ne devrait pas �tre un d�part", p.estDepart(-1));
	}

	/**
	 * Test de la r�cup�ration de l'indice de la Piste o� se trouve un Coureur
	 */
	@Test
	public void testGetIndex() {
		//preparation des donn�es
		String[] tuiles = {"LD","VS","VL","MV","MF"};
		Coureur c = new Coureur("Jascque", 1);
		Coureur faux = new Coureur("Rick",2);
		Piste p = new Piste(tuiles);
		p.getTuile(3).setVoieDroite(c);
		//validation du r�sultat
		assertEquals("La methode devrait retourner 3", p.getIndex(c), 3);
		assertEquals("La methode devrait retourner -1", p.getIndex(null), -1);
		assertEquals("La methode devrait retourner -1", p.getIndex(faux), -1);

	}

}
