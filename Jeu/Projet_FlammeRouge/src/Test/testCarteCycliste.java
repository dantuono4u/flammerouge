package Test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Class.CarteCycliste;
import Class.CarteSprinter;

public class testCarteCycliste {
	/**
	 * Test de la methode ajoutFatigue
	 */
	@Test
	public void testAjoutFatigue() {
		//preparation des donn�es
		CarteCycliste cc = new CarteSprinter();
		//methode teste
		cc.ajoutFatigue();
		//validation
		int t = cc.getDefausse().size();
		int carte = -1;
		if(t > 0) {
			carte = cc.getDefausse().get(0);
		}
		assertEquals("La taille de la defausse devrait etre de 1", 1, t);
		assertEquals("La carte ajoutee devrait etre de valeur 2", 2, carte);
	}
	
	/**
	 * Test de la methode nouvellePioche
	 */
	@Test
	public void testNouvellePioche() {
		//preparation des donn�es
		CarteCycliste cc = new CarteSprinter();
		cc.getPioche().clear();
		cc.getPioche().add(1);
		cc.ajoutFatigue();
		cc.ajoutFatigue();
		//methode teste
		cc.nouvellePioche();
		//validation
		int t = cc.getPioche().size();
		int somme = 0;
		for (int i = 0; i < cc.getPioche().size(); i++) {
			somme += cc.getPioche().get(i);
		}
		assertEquals("La taille de la pioche devrait etre de 3", 3, t);
		assertEquals("La somme de la valeur des cartes devrait etre de 5", 5, somme);
	}
	
	
	/**
	 * Test de la methode piocher
	 */
	@Test
	public void testPiocher() {
		//preparation des donnees
		CarteCycliste cc = new CarteSprinter();
		cc.getPioche().clear();
		cc.getPioche().add(1);
		cc.getPioche().add(2);
		//methode tester
		int res1 = cc.piocher();
		int res2 = cc.piocher();
		//validation des resultat
		assertEquals("La valeur de la premiere carte devrait etre de 1", 1, res1);
		assertEquals("La valeur de la premiere carte devrait etre de 2", 2, res2);
	}
	
	/**
	 * Test de la methode defausser
	 */
	@Test
	public void testDefausser() {
		//preparation des donn�es
				CarteCycliste cc = new CarteSprinter();
				//methode tester
				cc.defausser(1);
				//validation des resultat
				int t = cc.getDefausse().size();
				int carte = 0;
				if(t > 0) {
					carte = cc.getDefausse().get(0);
				}
				assertEquals("La taille de la defausse devrait etre de 1", 1, t);
				assertEquals("La valeur de la carte dans la defausse devrait etre de 1", 1, carte);
	}
}
