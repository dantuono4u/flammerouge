package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Class.*;

public class testLigneDroite {


	/**
	 * Test du Constructeur
	 */
	@Test
	public void testConstructeur() {
		//preparation des donn�es
		LigneDroite t = new LigneDroite();
		//methode test�e
		//validation du r�sultat
		assertEquals("La voie de gauche devrait �tre vide", t.getVoieGauche(), null);
		assertEquals("La voie de droite devrait �tre vide", t.getVoieDroite(), null);
	}

	/**
	 * Test du Setter de la voie de gauche de la Tuile
	 */
	@Test
	public void TestSetVoieGauche() {
		//preparation des donn�es
		LigneDroite t = new LigneDroite();
		Coureur c = new Coureur("test", 1);
		//methode test�e
		t.setVoieGauche(c);
		//validation du r�sultat
		assertEquals("La voie de gauche devrait contenir le Coureur c", t.getVoieGauche(), c);
		assertEquals("La voie de droite devrait �tre vide", t.getVoieDroite(), null);
	}

	/**
	 * Test du Setter de la voie de droite de la Tuile
	 */
	@Test
	public void TestSetVoieDroite() {
		//preparation des donn�es
		LigneDroite t = new LigneDroite();
		Coureur c = new Coureur("test", 1);
		//methode test�e
		t.setVoieDroite(c);
		//validation du r�sultat
		assertEquals("La voie de gauche devrait �tre vide", t.getVoieGauche(), null);
		assertEquals("La voie de droite devrait contenir le Coureur c", t.getVoieDroite(), c);
	}

	/**
	 * Test du Setter de la voie de gauche de la Tuile
	 */
	@Test
	public void TestToString() {
		//preparation des donn�es
		LigneDroite vide = new LigneDroite();
		LigneDroite gauche = new LigneDroite();
		LigneDroite droite = new LigneDroite();
		LigneDroite lesDeux = new LigneDroite();
		Coureur c = new Coureur("test", 1);
		//methode test�e
		gauche.setVoieGauche(c);
		droite.setVoieDroite(c);
		lesDeux.setVoieGauche(c);
		lesDeux.setVoieDroite(c);
		//validation du r�sultat
		assertEquals("La voie de gauche devrait �tre vide", vide.getVoieGauche(), null);
		assertEquals("La voie de droite devrait �tre vide", vide.getVoieDroite(), null);
		assertEquals("La voie de gauche devrait contenir le coureur c", gauche.getVoieGauche(), c);
		assertEquals("La voie de droite devrait �tre vide", gauche.getVoieDroite(), null);
		assertEquals("La voie de gauche devrait �tre vide", droite.getVoieGauche(), null);
		assertEquals("La voie de droite devrait contenir le coureur c", droite.getVoieDroite(), c);
		assertEquals("La voie de gauche devrait contenir le coureur c", lesDeux.getVoieGauche(), c);
		assertEquals("La voie de droite devrait contenir le coureur c", lesDeux.getVoieDroite(), c);
	}
}
