package Class;

import  javax.swing .*;
import  java.awt .*;
import java.io.IOException;
import  java.util .*;

public  class  Principale {
	public  static  void  main(String [] args) {
		System.out.println("---------Bienvenu chez Flamme Rouge---------");
		System.out.println("Ici, tu dois d�pacer tes limites en jouant \n"
				         + "contre tes amis. Ton but, finir le premier �\n"
				         + "une course de v�lo. Mais attention, la \n"
				         + "t�che ne sera pas si facile, tu vas devoir \n"
				         + "passer par de longues lignes droites mais \n"
				         + "malheuresement des mont�es aussi. Dans \n"
				         + "certains parcours, tu auras la chance de \n"
				         + "pouvoir te reposer dans les descentes. \n"
				         + "Aller, que le meilleur gagne !\n\n\n\n");
		System.out.println("-------------D�roulement du jeu-------------");
		System.out.println("Apr�s avoir choisi et plac� vos joueurs, \n"
				         + "vous allez choisir � tour de role, 1 carte \n"
				         + "parmis 4 de votre pioche. Elle repr�sentera \n"
				         + "votre d�placement en nombre de case. Une fois\n"
				         + "que tout le monde � choisisa carte, vos \n"
				         + "joueurs pourons faire la course. Vient \n"
				         + "ensuite, la phase de la fatigue, certains \n"
				         + "vont donc recevoir une carte fatigue dans \n"
				         + "leur pioche, les destabilisants aisni. D�s \n"
				         + "qu'un joueur aura franchit l'arriv�, \n"
				         + "la partie sera fin � la fin de ce m�me tour.\n\n\n\n\n");
		Jeu jeu = new Jeu();
		JFrame  fenetre=new  JFrame("Affichage Piste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
		Affichage  dessin=new  Affichage(jeu, 800,400);
		dessin.setBackground(new Color(23, 101, 125));
		fenetre.setContentPane(dessin);
		fenetre.pack ();
		fenetre.setVisible(true);
		while(jeu.isContinu()){
			System.out.println("\n->Tirage des cartes : ");
			jeu.AffichagePiste();
			//Phase de choix de la carte d'�nergie
			jeu.phaseE();
			System.out.println("\n->Mouvement");
			jeu.AffichagePiste();
			//Phase de mouvement
			jeu.phaseM();
			jeu.AffichagePiste();
			int posPremier = jeu.getClassement().getPremier().getPosition();
			if(jeu.getPiste().getTuile(posPremier) instanceof Arrivee) {
				jeu.finPartie(jeu.getClassement().getPremier());
			}else {
				System.out.println("\n->Aspiration");
				//Phase d'Aspiration
				jeu.Aspiration();
				jeu.AffichagePiste();
				System.out.println("\n->Ajout des cartes fatigues : ");
				//Phase Fatigue
				jeu.ajoutCarteFatigue();
				jeu.getClassement().miseAjour();
				posPremier = jeu.getClassement().getPremier().getPosition();
				dessin.repainte();
				jeu.clearConsole();
				//si un joueur � fini la course, fin de la partie
				if(jeu.getPiste().getTuile(posPremier) instanceof Arrivee) {
					jeu.finPartie(jeu.getClassement().getPremier());
				}}
			}
			
		dessin.repainte();
	}
}