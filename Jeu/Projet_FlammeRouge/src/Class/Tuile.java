package Class;

/**
 * Classe Tuile
 */
public interface Tuile {

	/**
	 * Getter de la voie de gauche de la Tuile
	 * @return Coureur retourne le coureur de la voie de Gauche
	 */
	public Coureur getVoieGauche();

	/**
	 * Setter de la voie de gauche de la Tuile
	 * @param voieGauche Coureur � placer � la voie de gauche
	 */
	public void setVoieGauche(Coureur voieGauche);

	/**
	 * Getter de la voie de Droite de la Tuile
	 * @return Coureur retourne le coureur de la voie de Gauche
	 */
	public Coureur getVoieDroite();

	/**
	 *Setter de la voie de droite de la Tuile 
	 *@param voieDroite Coureur � placer � la voie de droite
	 */
	public void setVoieDroite(Coureur voieDroite);

	@Override
	public String toString();
	
}
