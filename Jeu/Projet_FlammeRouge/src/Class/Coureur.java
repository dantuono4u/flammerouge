package Class;

/**
 * Classe Coureur
 **/
public class Coureur{
	//Attributs du Coureur
	private String type;
	private int couleur;
	private CarteCycliste listeCartes;
	private int position;
	private char voie;
	
	/**
	 * Constructeur
	 * @param t c information du coureur (Nom/Couleur)
	 **/
	public Coureur(String t, int c) {
		type = t;
		couleur = c;
		if(t.equals("s")){
			this.listeCartes = new CarteSprinter();
		}else{
			this.listeCartes = new CarteRouleur();
		}
	}

	
	@Override
	public String toString() {
		String r;
		if(this.type.equals("S")) {
			r = Couleur.CouleurS[this.couleur];
		}else if(this.type.equals("R")){
			r = Couleur.CouleurR[this.couleur];
		}else {
			r= "none";
		}
		return r;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + couleur;
		result = prime * result + position;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + voie;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coureur other = (Coureur) obj;
		if (couleur != other.couleur)
			return false;
		if (position != other.position)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (voie != other.voie)
			return false;
		return true;
	}


	/**
	 * Getter du Type du Coureur
	 * @return String Type du Coureur
	 **/
	public String getType() {
		return this.type;
	}
	
	/**
	 * Getter de la pioche du Coureur
	 * @return CarteCycliste 
	 **/
	public CarteCycliste getListeCartes() {
		return this.listeCartes;
	}
	
	/**
	 * Getter de la Position du Joueur
	 * @return position du coureur
	 **/
	public int getPosition() {
		return position;
	}
	
	/**
	 * Setter de la voie du coureur
	 * @param c 
	 **/
	public void setVoie(char c) {
		this.voie = c;
	}
	
	/**
	 *Getter de la voie du coureur 
	 *@return char 
	 **/
	public char getVoie() {
		return this.voie;
	}

	/**
	 * Setter de la Position du Joueur
	 * @param position nouvelle position
	 **/
	public void setPosition(int position) {
		this.position = position;
	}
	
	/**
	 * Getter de la couleur du Joueur
	 * @return int
	 **/
	public int getCouleur() {
		return this.couleur;
	}
	
	
}
