package Class;

public class LigneDroite implements Tuile{
	//Attributs correspondant au contenu de la voie de droite ou de gauche de la case
		private Coureur voieGauche,voieDroite;
		
		/**
		 * Constructeur
		 */
		public LigneDroite() {
			this.voieDroite = null;
			this.voieGauche = null;
		}

		/**
		 * Getter de la voie de gauche de la Tuile
		 */
		public Coureur getVoieGauche() {
			return voieGauche;
		}

		/**
		 * Setter de la voie de gauche de la Tuile
		 */
		public void setVoieGauche(Coureur voieGauche) {
			this.voieGauche = voieGauche;
		}

		/**
		 * Getter de la voie de Droite de la Tuile
		 */
		public Coureur getVoieDroite() {
			return voieDroite;
		}

		/**
		 *Setter de la voie de droite de la Tuile 
		 */
		public void setVoieDroite(Coureur voieDroite) {
			this.voieDroite = voieDroite;
		}

		@Override
		public String toString() {
			return voieGauche + "/ " + voieDroite;
		}
		
}
