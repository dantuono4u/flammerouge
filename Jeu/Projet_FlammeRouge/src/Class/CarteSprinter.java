package Class;

import java.util.ArrayList;
import java.util.Collections;



public class CarteSprinter extends CarteCycliste{
	
	
	public CarteSprinter(){
		super();
		pioche=new ArrayList<Integer>();
		defausse = new ArrayList<Integer>();
		creationPioche();
	}
	
	public void creationPioche() {
		//Initialisation de la pioche du Sprinteur
				for(int i=2; i<6; i++){
					for(int j=0; j<nbcartes; j++)
						pioche.add(i);
				}
				for(int j=0; j<nbcartes; j++)
					pioche.add(9);
				//M�lange de la Pioche
				Collections.shuffle(pioche);
	}
	
	public int somme(int[] tab){
		int s=0;
		for(int i=0; i<tab.length; i++){
			s+=tab[i];
		}
		return s;
	}
	
	
}

