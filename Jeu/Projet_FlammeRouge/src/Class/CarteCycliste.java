package Class;

import java.util.ArrayList;
import java.util.Collections;

public abstract class CarteCycliste {
	protected ArrayList<Integer> pioche, defausse;
	protected int nbcartes;
	protected int carteEnergie;
	public static final int fatigue =2;
	
	public CarteCycliste() {
		this.pioche = new ArrayList<Integer>();
		this.defausse = new ArrayList<Integer>();
		this.nbcartes = 3;
	}
	public void ajoutFatigue(){
		defausse.add(CarteCycliste.fatigue);
	}
	
	public void nouvellePioche(){
		Collections.shuffle(defausse);
		pioche.addAll(defausse);
		defausse=new ArrayList<Integer>();
	}
	
	public void setCarteE(int e){
		carteEnergie = e;
	}
	public int getCarteE(){
		return carteEnergie;
	}
	public int piocher(){
		if(pioche.size() < 4)
			nouvellePioche();
		int carte=pioche.get(0);
		pioche.remove(0);
		return carte;
	}
	
	public void defausser(int e){
		defausse.add(e);
	}
	
	public ArrayList<Integer> getDefausse(){
		return this.defausse;
	}
	
	public ArrayList<Integer> getPioche(){
		return this.pioche;
	}
	
	abstract void creationPioche();
}
