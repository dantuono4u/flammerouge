package Class;

/**
 * Classe Depart
 * Commencement de la Piste
 */
public class Depart extends LigneDroite{
	//Attribut constituant les Tuiles de d�part
	private LigneDroite[] place;
	
	/**
	 * Constructeur 
	 */
	public Depart(){
		super();
		this.place = new LigneDroite[4];
		for(int i=0;i<4; i++)
			place[i] = new LigneDroite();
	}

	/**
	 * Getter d'une Tuile du Depart
	 * @param i indice du Depart choisi
	 * @return Tuile tuile choisi par l'indice
	 */
	public LigneDroite getTuileDepart(int i) {
		return this.place[i+3];
	}
	
	/**
	 * M�thode permettant de placer les Coureurs au Depart
	 * @param c Coureur � placer
	 * @param place place du Coureur � placer
	 * @return Boolean retourne vrai si le Coureur est bien plac�
	 */
	public boolean placer(Coureur c, int place){
		boolean fait=false;
		//placemant en fonction de l'indice choisie et s'il y a une place
		switch (place) {
		case 1:
			if(this.place[0].getVoieGauche()==null){
				this.place[0].setVoieGauche(c);
				c.setPosition(-3);
				c.setVoie('g');
				return true;
			}
			break;
			
		case 2:
			if(this.place[0].getVoieDroite()==null){
				this.place[0].setVoieDroite(c);
				c.setPosition(-3);
				c.setVoie('d');
				return true;
			}
			break;
			
		case 3:
			if(this.place[1].getVoieGauche()==null){
				this.place[1].setVoieGauche(c);
				c.setPosition(-2);
				c.setVoie('g');
				return true;
			}
			break;
			
		case 4:
			if(this.place[1].getVoieDroite()==null){
				this.place[1].setVoieDroite(c);
				c.setPosition(-2);
				c.setVoie('d');
				return true;
			}
			break;
			
		case 5:
			if(this.place[2].getVoieGauche()==null){
				this.place[2].setVoieGauche(c);
				c.setPosition(-1);
				c.setVoie('g');
				return true;
			}
			break;
			
		case 6:
			if(this.place[2].getVoieDroite()==null){
				this.place[2].setVoieDroite(c);
				c.setPosition(-1);
				c.setVoie('d');
				return true;
			}
			break;
			
		case 7:
			if(this.place[3].getVoieGauche()==null){
				this.place[3].setVoieGauche(c);
				c.setPosition(0);
				c.setVoie('g');
				return true;
			}
			break;
			
		case 8:
			if(this.place[3].getVoieDroite()==null){
				this.place[3].setVoieDroite(c);
				c.setPosition(0);
				c.setVoie('d');
				return true;
			}
			break;
		}
		
		return fait;
		
	}
	/**
	 * Getter du Tableau de Tuile correspondant au Depart
	 * @return Tuile[] Depart sous forme d'un tableau de Tuile
	 */
	public LigneDroite[] getDepart(){
		return place;
	}
	
	/**
	 * M�thode permettant d'afficher l'�tat du Depart dans la console
	 */
	public void afficherDepart() {
		String gauche = "|";
		String droite = "|";
		for (int i = 0; i < 4; i++) {
			if(this.place[i].getVoieGauche() == null) {
				gauche = gauche + "   (" + i + ")   |";
			}else {
				gauche = gauche + this.place[i].getVoieGauche();
			}
			
			if(this.place[i].getVoieDroite() == null) {
				droite = droite + "   (" + i + ")   |";
			}else {
				droite = droite + this.place[i].getVoieDroite();
			}	
		}
		String ligne = "";
		for (int i = 0; i < gauche.length(); i++) {
			ligne += "-";
		}
		
		System.out.println(ligne);
		System.out.println(gauche + "\\");
		System.out.println(droite + "/");
		System.out.println(ligne);
		
	}
	
	
	@Override
	public String toString() {
		String str="-----------Depart-----------\n";
		for(int i=0; i<4;i++)
			str+= i+":"+place[i].toString() + "\n";
		return str;
	}
	
	
}
