package Class;

import java.awt.Color;

public class Couleur {
	//Atributs Statics correspondants au couleurs
	public static int NOIR = 0; 
	public static int CYAN = 1;
	public static int VERT = 2;
	public static int PINK = 3;
	//Atributs tableau pour les affichages
	public static String[] tabCouleur = {"Noir","Cyan","Vert","Rose"};
	public static String[] CouleurR = {" R(Noir)"," R(Cyan)"," R(Vert)"," R(Pink)"};
	public static String[] CouleurS = {" S(Noir)"," S(Cyan)"," S(Vert)"," S(Pink)"};
	
	public static Color[] color =  {Color.black, Color.cyan, Color.green, Color.pink};
	
}
